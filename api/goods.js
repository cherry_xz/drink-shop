/** 
 * 前端封装API接口模块 ：商品(Goods) 模块
 */
import request from '@/utils/request' // 引入 request
const prefix = "/goods"


export function goodsMenuList() {
	return request({
		method: "GET",
		url: prefix + "/goodsMenu/list",
	})
}
import {
	baseUrl
} from "@/utils/config.js"
import request from '@/utils/request' // 引入 request
/* 文件上传接口封装 file.js */
const prefix = "/common"

export function uploadFile(file) {
	return uni.uploadFile({
		url: baseUrl + prefix + "/upload",
		file: file,
	}).then(result => {
		console.log(result)
		result = JSON.parse(result[1].data)
		if (result.code === 200) {
			return Promise.resolve(result); //返回成功 
		}
		console.log(result.msg)
		return Promise.reject('uni.uploadFile调用失败')
	})
}

export function uploadFile2(file) {
	let config = {
		headers: {
			'content-type': 'multipart/form-data'
		},
	}
	let formData = new FormData()
	formData.append("file", file);
	console.log(formData)
	return request({
		url: prefix + "/upload",
		method: "post",
		data: formData,
		config: config,
	});
}
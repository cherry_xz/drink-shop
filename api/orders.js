/** 
 * 前端封装API接口模块 ：
 */
import request from '@/utils/request' // 引入 request
const prefix = "/order"


export function createOrder(params) {
	return request({
		method: "POST",
		url: "/order/create",
		data: params
	})
}

// 未完成订单列表
export function orderNotCompleted() {
	return request({
		method: "GET",
		url: "/order/notCompleted",
	})
}

// 取消订单
export function orderCancel(orderNo) {
	return request({
		method: "delete",
		url: "/order/cancel/" + orderNo,
	})
}

// 确认收货
export function orderConfirmReceive(orderNo) {
	return request({
		method: "put",
		url: "/order/confirmReceive/" + orderNo,
	})
}

// 模拟支付
export function orderPayMP(orderNo) {
	return request({
		method: "get",
		url: "/order/payMP/" + orderNo,
	})
}

// h5收银台的支付方式
export function orderPayCashier(orderNo) {
	return request({
		method: "get",
		url: "/order/payCashier/" + orderNo,
	})
}

// 订单详情信息
export function orderDetail(orderNo) {
	return request({
		method: "get",
		url: "/order/detail/" + orderNo,
	})
}

// 分页获取历史订单
export function orderHistoryPage(pageNo, pageSize) {
	return request({
		method: "get",
		url: "/order/history/page/" + pageNo + '/' + pageSize
	})
}

// 分页获取历史订单
export function orderFinishWeixinPay(params) {
	return request({
		method: "put",
		url: "/order/finishWeixinPay",
		data: params,
	})
}
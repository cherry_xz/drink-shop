/** 
 * 前端封装API接口模块 ：用户(User) 模块
 */
import request from '@/utils/request' // 引入 request
const prefix = "/user"

/**用户登录
 * @param {Object} params
 * {
  "name": "",
  "password": ""
}
 */
export function login(params) {
	return request({
		method: "POST",
		url: prefix + "/login",
		data: params
	})
}

/**用户登出
 * 
 */
export function logout() {
	return request({
		method: "POST",
		url: prefix + "/logout",
		data: params
	})
}

/**用户注册
 * @param {Object} params
  {
  "avatar": "",
  "email": "",
  "gender": "",
  "name": "",
  "nickName": "",
  "password": "",
  "phone": "",
  "remark": ""
	}
 */
export function register(params) {
	return request({
		method: "POST",
		url: prefix + "/register",
		data: params
	})
}


/**用户更新 By 用户ID
 * @param {Object} params
 * {
  "avatar": "",
  "email": "",
  "gender": 0,
  "id": 0,
  "name": "",
  "nickName": "",
  "password": "",
  "phone": ""
}
 */
export function update(params) {
	return request({
		method: "POST",
		url: prefix + "/update",
		data: params
	})
}

/**用户修改密码 By用户Id 看后端提供的接口文档
 * @param {Object} params
 * {
  "id": 0,
  "passwordNew": "",
  "passwordOld": ""
}
 */
export function updatePwd(params) {
	return request({
		method: "POST",
		url: prefix + "/updatePwd",
		data: params
	})
}

/**查询登录用户详情 
 * 
 */
export function getMyDetail() {
	return request({
		method: "GET",
		url: prefix + "/detail",
	})
}
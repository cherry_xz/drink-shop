import packages from './packages'
import attendance from './attendance'
import attendanceList from './attendance-list'
import todayAttendance from './today-attendance'
import customPoints from './custom-points'
import pointsMall from './points-mall'
import pointsFlow from './points-flow'

import rechargeCards from './rechargeCards'
import customerCoupons from './customer-coupons'
import giftCards from './gift-cards'

const json = {
	packages,
	attendance,
	attendanceList,
	todayAttendance,
	customPoints,
	pointsMall,
	pointsFlow,

	rechargeCards,
	customerCoupons,
	giftCards
}

export default (name) => new Promise(resolve => resolve(json[name]), 500)
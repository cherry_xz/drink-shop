import Vue from 'vue'
import App from './App'
import util from './common/util'
import store from './store'
import config from './config.js'
import api from './api/index.js' //前端静态Json数据接口

import { createSSRApp } from "vue";

export function createApp() {
	const app = createSSRApp(App);

	return {
		app,
	};
}


// 统一提示
function msg(title = "", duration = 1000, icon = 'none', mask = false) {
	uni.showToast({
		title: title,
		duration: duration,
		icon: icon, // none success loading
		mask: mask
	});
}

const prePage = function() {
	let pages = getCurrentPages();
	let prePage = pages[pages.length - 2];
}

// uview-ui插件
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

import NavCustom from 'components/nav-custom.vue'
Vue.component('nav-custom', NavCustom)
import HomeTitle from 'components/home-title.vue'
Vue.component('home-title', HomeTitle)
import GoodItem from 'components/good-item.vue'
Vue.component('good-item', GoodItem)

// 挂载全局方法
Vue.config.productionTip = false
// Vue.prototype.$request = request;
Vue.prototype.$msg = msg;
Vue.prototype.$prePage = prePage
Vue.prototype.$store = store
Vue.prototype.$util = util
Vue.prototype.$config = config


Vue.prototype.$backUrl = 'http://localhost:5050'

Vue.prototype.$api = api //前端静态Json数据接口

App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
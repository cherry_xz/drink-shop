# 标题：若依管理系统_接口文档


**简介**:标题：若依管理系统_接口文档


**HOST**:http://localhost:81


**联系人**:RuoYi


**Version**:版本号:3.8.8


**接口路径**:/v3/api-docs


[TOC]






# 02.用户接口


## 查询我的详情


**接口地址**:`/dev-api/user/detail`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:<p>查询当前登录用户的详情信息</p>



**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«用户详情返回数据»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||用户详情返回数据|用户详情返回数据|
|&emsp;&emsp;avatar|用户头像|string||
|&emsp;&emsp;email|邮箱|string||
|&emsp;&emsp;gender|性别|integer(int32)||
|&emsp;&emsp;id|用户id|integer(int64)||
|&emsp;&emsp;name|用户名|string||
|&emsp;&emsp;nickName|用户昵称|string||
|&emsp;&emsp;phone|手机|string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"avatar": "",
		"email": "",
		"gender": 0,
		"id": 0,
		"name": "",
		"nickName": "",
		"phone": ""
	},
	"msg": ""
}
```


## 用户登录


**接口地址**:`/dev-api/user/login`


**请求方式**:`POST`


**请求数据类型**:`application/x-www-form-urlencoded,application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "name": "",
  "password": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userLoginForm|UserLoginForm|body|true|UserLoginForm|UserLoginForm|
|&emsp;&emsp;name|用户名||true|string||
|&emsp;&emsp;password|用户密码||true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«用户登录返回数据»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||用户登录返回数据|用户登录返回数据|
|&emsp;&emsp;address|地址|string||
|&emsp;&emsp;avatar|用户头像|string||
|&emsp;&emsp;id|用户id|integer(int64)||
|&emsp;&emsp;name|用户Name|string||
|&emsp;&emsp;nickName|昵称|string||
|&emsp;&emsp;phone|手机|string||
|&emsp;&emsp;token|Token|string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"address": "",
		"avatar": "",
		"id": 0,
		"name": "",
		"nickName": "",
		"phone": "",
		"token": ""
	},
	"msg": ""
}
```


## 用户登出


**接口地址**:`/dev-api/user/logout`


**请求方式**:`POST`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:<p>清除token</p>



**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## 用户注册


**接口地址**:`/dev-api/user/register`


**请求方式**:`POST`


**请求数据类型**:`application/x-www-form-urlencoded,application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "address": "",
  "avatar": "",
  "email": "",
  "gender": "",
  "name": "",
  "nickName": "",
  "password": "",
  "phone": "",
  "remark": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userRegisterForm|UserRegisterForm|body|true|UserRegisterForm|UserRegisterForm|
|&emsp;&emsp;address|地址||true|string||
|&emsp;&emsp;avatar|头像||false|string||
|&emsp;&emsp;email|邮箱地址||true|string||
|&emsp;&emsp;gender|性别||false|string(byte)||
|&emsp;&emsp;name|用户名||true|string||
|&emsp;&emsp;nickName|昵称||false|string||
|&emsp;&emsp;password|用户密码||true|string||
|&emsp;&emsp;phone|手机号码||true|string||
|&emsp;&emsp;remark|备注||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«用户注册返回数据»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||用户注册返回数据|用户注册返回数据|
|&emsp;&emsp;avatar|用户头像|string||
|&emsp;&emsp;id|用户id|integer(int64)||
|&emsp;&emsp;name|用户Name|string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"avatar": "",
		"id": 0,
		"name": ""
	},
	"msg": ""
}
```


## 更改用户信息


**接口地址**:`/dev-api/user/update`


**请求方式**:`POST`


**请求数据类型**:`application/x-www-form-urlencoded,application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>用户id不能为空</p>



**请求示例**:


```javascript
{
  "address": "",
  "avatar": "",
  "email": "",
  "gender": 0,
  "id": 0,
  "name": "",
  "nickName": "",
  "password": "",
  "phone": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userUpdateForm|UserUpdateForm|body|true|UserUpdateForm|UserUpdateForm|
|&emsp;&emsp;address|||false|string||
|&emsp;&emsp;avatar|用户头像||false|string||
|&emsp;&emsp;email|邮箱||false|string||
|&emsp;&emsp;gender|性别||false|integer(int32)||
|&emsp;&emsp;id|用户id||true|integer(int64)||
|&emsp;&emsp;name|用户名||false|string||
|&emsp;&emsp;nickName|用户昵称||false|string||
|&emsp;&emsp;password|用户密码||false|string||
|&emsp;&emsp;phone|手机||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## 更改用户密码


**接口地址**:`/dev-api/user/updatePwd`


**请求方式**:`POST`


**请求数据类型**:`application/x-www-form-urlencoded,application/json`


**响应数据类型**:`*/*`


**接口描述**:<p>用户id，新旧密码不能为空</p>



**请求示例**:


```javascript
{
  "id": 0,
  "passwordNew": "",
  "passwordOld": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userUpdatePwdForm|UserUpdatePwdForm|body|true|UserUpdatePwdForm|UserUpdatePwdForm|
|&emsp;&emsp;id|用户id||true|integer(int64)||
|&emsp;&emsp;passwordNew|新密码||true|string||
|&emsp;&emsp;passwordOld|原密码||true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


# test-controller


## 获取用户列表


**接口地址**:`/dev-api/test/user/list`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«List«UserEntity»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||array|UserEntity|
|&emsp;&emsp;mobile|用户手机|string||
|&emsp;&emsp;password|用户密码|string||
|&emsp;&emsp;userId|用户ID|integer(int32)||
|&emsp;&emsp;username|用户名称|string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"mobile": "",
			"password": "",
			"userId": 0,
			"username": ""
		}
	],
	"msg": ""
}
```


## 新增用户


**接口地址**:`/dev-api/test/user/save`


**请求方式**:`POST`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|mobile|用户手机|query|false|string||
|password|用户密码|query|false|string||
|userId|用户id|query|false|integer(int32)||
|username|用户名称|query|false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## 更新用户


**接口地址**:`/dev-api/test/user/update`


**请求方式**:`PUT`


**请求数据类型**:`application/x-www-form-urlencoded,application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求示例**:


```javascript
{
  "mobile": "",
  "password": "",
  "userId": 0,
  "username": ""
}
```


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userEntity|用户实体|body|true|UserEntity|UserEntity|
|&emsp;&emsp;mobile|用户手机||false|string||
|&emsp;&emsp;password|用户密码||false|string||
|&emsp;&emsp;userId|用户ID||false|integer(int32)||
|&emsp;&emsp;username|用户名称||false|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«string»|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## 获取用户详细


**接口地址**:`/dev-api/test/user/{userId}`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userId|用户ID|path|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«UserEntity»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||UserEntity|UserEntity|
|&emsp;&emsp;mobile|用户手机|string||
|&emsp;&emsp;password|用户密码|string||
|&emsp;&emsp;userId|用户ID|integer(int32)||
|&emsp;&emsp;username|用户名称|string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"mobile": "",
		"password": "",
		"userId": 0,
		"username": ""
	},
	"msg": ""
}
```


## 删除用户信息


**接口地址**:`/dev-api/test/user/{userId}`


**请求方式**:`DELETE`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|userId|用户ID|path|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«string»|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


# 商品服务


## 获取要显示的商品菜单列表


**接口地址**:`/dev-api/goods/goodsMenu/list`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«List«商品菜单»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||array|商品菜单|
|&emsp;&emsp;displayOrder||integer(int32)||
|&emsp;&emsp;goodsCategoryName||string||
|&emsp;&emsp;goodsCategoryShow||boolean||
|&emsp;&emsp;goodsList||array|商品|
|&emsp;&emsp;&emsp;&emsp;defaultPrice||integer||
|&emsp;&emsp;&emsp;&emsp;description||string||
|&emsp;&emsp;&emsp;&emsp;displayOrder||integer||
|&emsp;&emsp;&emsp;&emsp;goodsCategoryName||string||
|&emsp;&emsp;&emsp;&emsp;goodsPropertyVos||array|SameCategoryPropertyVO|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;category||string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;propertyList||array|商品的属性，温度甜度等|
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;category||string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;extraPrice||integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;goodsId||integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;id||integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;isDefault||boolean||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;propertyOption||string||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;rebasePrice||integer||
|&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;required||boolean||
|&emsp;&emsp;&emsp;&emsp;id||integer||
|&emsp;&emsp;&emsp;&emsp;image||string||
|&emsp;&emsp;&emsp;&emsp;isSell||boolean||
|&emsp;&emsp;&emsp;&emsp;name||string||
|&emsp;&emsp;&emsp;&emsp;number||integer||
|&emsp;&emsp;&emsp;&emsp;propertyStr||string||
|&emsp;&emsp;&emsp;&emsp;realPrice||integer||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"displayOrder": 0,
			"goodsCategoryName": "",
			"goodsCategoryShow": true,
			"goodsList": [
				{
					"defaultPrice": 0,
					"description": "",
					"displayOrder": 0,
					"goodsCategoryName": "",
					"goodsPropertyVos": [
						{
							"category": "",
							"propertyList": [
								{
									"category": "",
									"extraPrice": 0,
									"goodsId": 0,
									"id": 0,
									"isDefault": true,
									"propertyOption": "",
									"rebasePrice": 0
								}
							],
							"required": true
						}
					],
					"id": 0,
					"image": "",
					"isSell": true,
					"name": "",
					"number": 0,
					"propertyStr": "",
					"realPrice": 0
				}
			]
		}
	],
	"msg": ""
}
```


# 订单服务


## 取消订单


**接口地址**:`/dev-api/order/cancel/{orderNo}`


**请求方式**:`DELETE`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|orderNo|订单号|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«string»|
|204|No Content||
|401|Unauthorized||
|403|Forbidden||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": "",
	"msg": ""
}
```


## 确认收货


**接口地址**:`/dev-api/order/confirmReceive/{orderNo}`


**请求方式**:`PUT`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|orderNo|订单号|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||object||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## 订单详情信息


**接口地址**:`/dev-api/order/detail/{orderNo}`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|orderNo|orderNo|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«订单»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||订单|订单|
|&emsp;&emsp;addressDetail||string||
|&emsp;&emsp;createTime||string(date-time)||
|&emsp;&emsp;extraInfo|用户备注|string||
|&emsp;&emsp;finishTime|订单完成时间|string(date-time)||
|&emsp;&emsp;goodsPreview|所有商品概览信息|string||
|&emsp;&emsp;goodsTotalNum||integer(int32)||
|&emsp;&emsp;orderNo||string||
|&emsp;&emsp;orderStatus||string||
|&emsp;&emsp;payPrice||integer(int32)||
|&emsp;&emsp;payTime|支付时间|string(date-time)||
|&emsp;&emsp;payjsOrderId||string||
|&emsp;&emsp;receiver|取餐人|string||
|&emsp;&emsp;takeType||string||
|&emsp;&emsp;totalPrice||integer(int32)||
|&emsp;&emsp;userPhone||string||
|&emsp;&emsp;verifyNum|取单号，一般取手机尾号|integer(int32)||
|&emsp;&emsp;wxOpenid||string||
|&emsp;&emsp;wxPayTransactionId|微信支付系统生成的订单号|string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"addressDetail": "",
		"createTime": "",
		"extraInfo": "",
		"finishTime": "",
		"goodsPreview": "",
		"goodsTotalNum": 0,
		"orderNo": "",
		"orderStatus": "",
		"payPrice": 0,
		"payTime": "",
		"payjsOrderId": "",
		"receiver": "",
		"takeType": "",
		"totalPrice": 0,
		"userPhone": "",
		"verifyNum": 0,
		"wxOpenid": "",
		"wxPayTransactionId": ""
	},
	"msg": ""
}
```


## 完成支付，并设置payjs的orderid


**接口地址**:`/dev-api/order/finishWeixinPay`


**请求方式**:`PUT`


**请求数据类型**:`application/x-www-form-urlencoded,application/json`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||object||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## 分页获取历史订单


**接口地址**:`/dev-api/order/history/page/{pageNo}/{pageSize}`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|pageNo|pageNo|path|true|integer(int32)||
|pageSize|pageSize|path|true|integer(int32)||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«Page«历史订单列表信息»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||Page«历史订单列表信息»|Page«历史订单列表信息»|
|&emsp;&emsp;current||integer(int64)||
|&emsp;&emsp;hitCount||boolean||
|&emsp;&emsp;orders||array|OrderItem|
|&emsp;&emsp;&emsp;&emsp;asc||boolean||
|&emsp;&emsp;&emsp;&emsp;column||string||
|&emsp;&emsp;pages||integer(int64)||
|&emsp;&emsp;records||array|历史订单列表信息|
|&emsp;&emsp;&emsp;&emsp;createTime||string||
|&emsp;&emsp;&emsp;&emsp;goodsPreview|商品大致信息，eg: 奶茶*2|string||
|&emsp;&emsp;&emsp;&emsp;goodsTotalNum||integer||
|&emsp;&emsp;&emsp;&emsp;orderNo||string||
|&emsp;&emsp;&emsp;&emsp;orderStatus||string||
|&emsp;&emsp;&emsp;&emsp;payPrice||integer||
|&emsp;&emsp;searchCount||boolean||
|&emsp;&emsp;size||integer(int64)||
|&emsp;&emsp;total||integer(int64)||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"current": 0,
		"hitCount": true,
		"orders": [
			{
				"asc": true,
				"column": ""
			}
		],
		"pages": 0,
		"records": [
			{
				"createTime": "",
				"goodsPreview": "",
				"goodsTotalNum": 0,
				"orderNo": "",
				"orderStatus": "",
				"payPrice": 0
			}
		],
		"searchCount": true,
		"size": 0,
		"total": 0
	},
	"msg": ""
}
```


## 获取正在处理的订单列表


**接口地址**:`/dev-api/order/notCompleted`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«List«订单»»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||array|订单|
|&emsp;&emsp;addressDetail||string||
|&emsp;&emsp;createTime||string(date-time)||
|&emsp;&emsp;extraInfo|用户备注|string||
|&emsp;&emsp;finishTime|订单完成时间|string(date-time)||
|&emsp;&emsp;goodsPreview|所有商品概览信息|string||
|&emsp;&emsp;goodsTotalNum||integer(int32)||
|&emsp;&emsp;orderNo||string||
|&emsp;&emsp;orderStatus||string||
|&emsp;&emsp;payPrice||integer(int32)||
|&emsp;&emsp;payTime|支付时间|string(date-time)||
|&emsp;&emsp;payjsOrderId||string||
|&emsp;&emsp;receiver|取餐人|string||
|&emsp;&emsp;takeType||string||
|&emsp;&emsp;totalPrice||integer(int32)||
|&emsp;&emsp;userPhone||string||
|&emsp;&emsp;verifyNum|取单号，一般取手机尾号|integer(int32)||
|&emsp;&emsp;wxOpenid||string||
|&emsp;&emsp;wxPayTransactionId|微信支付系统生成的订单号|string||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": [
		{
			"addressDetail": "",
			"createTime": "",
			"extraInfo": "",
			"finishTime": "",
			"goodsPreview": "",
			"goodsTotalNum": 0,
			"orderNo": "",
			"orderStatus": "",
			"payPrice": 0,
			"payTime": "",
			"payjsOrderId": "",
			"receiver": "",
			"takeType": "",
			"totalPrice": 0,
			"userPhone": "",
			"verifyNum": 0,
			"wxOpenid": "",
			"wxPayTransactionId": ""
		}
	],
	"msg": ""
}
```


## 接收支付返回的消息


**接口地址**:`/dev-api/order/pay/notify`


**请求方式**:`POST`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R|
|201|Created||
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||object||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


## 获取payJS支付小程序支付需要的支付参数信息


**接口地址**:`/dev-api/order/payMP/{orderNo}`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


| 参数名称 | 参数说明 | 请求类型    | 是否必须 | 数据类型 | schema |
| -------- | -------- | ----- | -------- | -------- | ------ |
|orderNo|orderNo|path|true|string||


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||object||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {},
	"msg": ""
}
```


# 首页轮播


## 获取首页轮播


**接口地址**:`/dev-api/home/index`


**请求方式**:`GET`


**请求数据类型**:`application/x-www-form-urlencoded`


**响应数据类型**:`*/*`


**接口描述**:


**请求参数**:


暂无


**响应状态**:


| 状态码 | 说明 | schema |
| -------- | -------- | ----- | 
|200|OK|R«HomeInfoVo»|
|401|Unauthorized||
|403|Forbidden||
|404|Not Found||


**响应参数**:


| 参数名称 | 参数说明 | 类型 | schema |
| -------- | -------- | ----- |----- | 
|code||integer(int32)|integer(int32)|
|data||HomeInfoVo|HomeInfoVo|
|&emsp;&emsp;carouselList|轮播图(列表)|array|HomeCarouselVo|
|&emsp;&emsp;&emsp;&emsp;goodsId|轮播图点击后跳转的商品id|integer||
|&emsp;&emsp;&emsp;&emsp;id|Id|integer||
|&emsp;&emsp;&emsp;&emsp;img|轮播图图片地址|string||
|&emsp;&emsp;&emsp;&emsp;sort|排序|integer||
|msg||string||


**响应示例**:
```javascript
{
	"code": 0,
	"data": {
		"carouselList": [
			{
				"goodsId": 0,
				"id": 0,
				"img": "",
				"sort": 0
			}
		]
	},
	"msg": ""
}
```
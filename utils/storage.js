const TokenKey = 'token' //键
//Token保存在SeessionStorage 
export function getStorageToken() {   //取数据
    return uni.getStorageSync(TokenKey)
}

export function setStorageToken(token) {  //存数据
    return uni.setStorageSync(TokenKey, token)
}

export function removeStorageToken() {   //清除数据
    return uni.removeStorageSync(TokenKey)
}


const UserKey = 'user'  //键
//用户数据保存在LocalStorage
export function getStorageUser() {
    let user = uni.getStorageSync(UserKey)
		if(typeof user =="string"){
			return user ? JSON.parse(user) : {}
		}else{
			return user
		}
    
}

export function setStorageUser(user) {
    return uni.setStorageSync(UserKey, JSON.stringify(user))
}

export function removeStorageUser() {
    return uni.removeStorageSync(UserKey)
}
// 导入配置的基地址
import {
	baseUrl
} from "@/utils/config.js"
//导入本地存储方法
import {
	getStorageToken,
	removeStorageToken
} from "@/utils/storage.js"

const timeout = 5000

const request = (config) => {
	// 每次请求都携带 token
	const token = getStorageToken();
	if (token) {
		config.header = config.header || {}
		config.header.satoken = token; // 参见后端API文档		
	}

	// 在发送请求之前做些什么
	uni.showLoading({
		title: '加载中...'
	});

	console.log(config)

	return uni.request({
		method: config.method || 'get',
		timeout: config.timeout || timeout,
		url: baseUrl + config.url,
		data: config.data,
		header: config.header
	}).then(result => {
		uni.hideLoading();
		console.log(result)
		result = result[1].data
		switch (result.code) {
			case 200:
				return Promise.resolve(result); //返回成功 
				break;
		}
		console.log(result)
		uni.showModal({
			title: '访问API出错',
			content: result.msg,
			showCancel: false
		});
		return

	}).catch(error => {
		uni.hideLoading();
		console.log(error)
		uni.showModal({
			title: 'uni.request调用错误',
			content: error.errMsg,
			showCancel: false
		});
		return Promise.reject('uni.request调用错误')
	})

}
export default request